# symfony-first-project

#Prerequisites
What things you need to install the software and how to install them

```
composer install
```
#Run Aplication
**Get entity and count of relation**

Please enter entity (users, posts), entity_id (1-10), relation (posts, comments) in url.
Urls given in below examples will store count of relations and “entity” information in jSON formate.

For examples:

```
http://127.0.0.1:8000/users/1/todos
```


```
http://127.0.0.1:8000/posts/2/comments
```

**Get list of entities**

Urls given in below examples will store list of “entities” information in jSON formate.

For examples:

```
http://127.0.0.1:8000/users
```


```
http://127.0.0.1:8000/posts
```

**Get entity information**

Urls given in below examples will store information of “entity” in jSON formate.

For examples:

```
http://127.0.0.1:8000/users/1
```


```
http://127.0.0.1:8000/posts/2
```
