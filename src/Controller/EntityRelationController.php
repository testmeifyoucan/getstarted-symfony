<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EntityRelationController extends AbstractController
{
    private $api;

    public function __construct()
    {
        $this->api = 'https://jsonplaceholder.typicode.com/';
    }


    /**
     * @Route("/entity/relation", name="entity_relation")
     */
    public function index()
    {
        return $this->render('entity_relation/index.html.twig', [
            'controller_name' => 'EntityRelationController',
        ]);
    }

    /**
     * @Route("/{entity?}/{entity_id?}/{relation?}", name="EntityRelation")
     */

    public function entity_relation($entity,$entity_id, $relation)
    {
        // Check if entity in the url exist
        if(!empty($entity)) {
            //New Guzzle Client
            $client = new \GuzzleHttp\Client();

            // Set Headers to create json file
            header('Content-disposition: attachment; filename=file.json');
            header('Content-type: application/json');

            $data = [];
            $data['message'] = "To get specific entity data. Please add entity_id in the url";

            // Get entity List of $entity
            $entities = $client->request('GET', $this->api.$entity);

            // If get request status is success
            if($entities->getStatusCode() == '200' || '201') {
                $entities = json_decode($entities->getBody()->getContents(), true);
                $data['entities'] = $entities;
            }
            //Check if entity id exist and is numeric.
            if(is_numeric($entity_id)&& !empty($entity_id)) {
                // Get specific entity data
                $response_entity = $client->request('GET', $this->api . $entity . '/' . $entity_id);

                // If get request status is success
                if ($response_entity->getStatusCode() == '200' || '201') {
                    $entityData = $response_entity->getBody();
                    $response_entity = json_decode($entityData, true);

                    //Check if $relation exist and is string.
                    if(is_string($relation) && !empty($relation)) {
                        // Get relations Data
                        $relations = $client->request('GET', $this->api. $relation );
                        // If get request status is success
                        if ($relations->getStatusCode() == '200' || '201'){

                            $relations = json_decode($relations->getBody());

                            //Separate foreign key of relation
                            $relation_param = key($relations[0]);

                            //Check if relation exist in entity
                            if ($relation_param == rtrim($entity, "s") . 'Id') {
                                // Get relations of entity
                                $relation = $client->request('GET', $this->api . $relation . '?' . $relation_param . '=' . $entity_id)->getBody();
                                // Count number of relations for entity
                                $rleation_count = count(json_decode($relation, true));

                                $response_entity['relation_count'] = $rleation_count;

                                //Return entity and count of relations
                                return new JsonResponse($response_entity);
                            }
                            // if relation not exist in entity
                            return new JsonResponse("Relationship between entity and relation is not exist");
                        }
                    }

                    //if $relation exist or not string return only entity of $entity_id
                    return new Response($entityData);
                }
            }

            //If entity_id is not numeric. Return list of entity
            return new JsonResponse($data);
        }

        // if entity is not exist exist
        return new Response('Please enter entity (users, posts), entity_id (1-10), relation (posts, comments) in url i.e. http://127.0.0.1:8000/entity_relation/users');
    }

}
